const express = require ('express')
const router = express.Router();
const auth = require('../auth')


const courseController = require('../controllers/courseController')

router.post('/', auth.verify, (request,response) => {

	const userData = auth.decode(request.headers.authorization)

	if(userData.isAdmin){
		courseController.addCourse(request.body).then(resultFromController => response.send(resultFromController))
	}

	
})



router.get('/all', auth.verify,(request,response) => {
	const userData = auth.decode(request.headers.authorization)

	courseController.getAllCourses(userData).then(resultFromController => response.send(resultFromController)) 
})

router.get('/', (request,response) => {
	courseController.getAllActive().then(resultFromController => response.send(resultFromController)) 
})


router.get('/:courseId', (request,response) => {
	console.log(request.params)

	courseController.getCourse(request.params).then(resultFromController => response.send(resultFromController))
})


router.put('/:courseId', auth.verify,(request,response) => {
	courseController.updateCourse(request.params,request.body).then(resultFromController => response.send(resultFromController))
})





router.put('/:archive', auth.verify,(request,response) => {
	
	courseController.courseArchive(request.params,request.body).then(resultFromController => response.send(resultFromController))
})


module.exports = router 